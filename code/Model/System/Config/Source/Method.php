<?php

class Mgcs_Sphinx_Model_System_Config_Source_Method
{

    public function toOptionArray()
    {
        $node = Mage::getConfig()->getNode('global/mgcs_sphinx/sources');

        $result = array();
        foreach ($node->children() as $method) {
            $result[] = array(
                'value' => (string)$method->model,
                'label' => (string)$method->title,
            );
        }
        return $result;
    }

}