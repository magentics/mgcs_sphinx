<?php

class Mgcs_Sphinx_Model_System_Config_Backend_Resetresults extends Mage_Core_Model_Config_Data
{
    /**
     * After change 'Replace' process
     *
     * @return Mgcs_Sphinx_Model_System_Config_Backend_Replace
     */
    protected function _afterSave()
    {
        $newValue = $this->getValue();
        $oldValue = Mage::getConfig()->getNode(
            $this->getPath(),
            $this->getScope(),
            $this->getScopeId()
        );
        if ($newValue != $oldValue) {
            Mage::getSingleton('catalogsearch/fulltext')->resetSearchResults();
        }

        return $this;
    }
}
