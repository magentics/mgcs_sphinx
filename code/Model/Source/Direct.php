<?php

require_once 'sphinxapi.php';

class Mgcs_Sphinx_Model_Source_Direct implements Mgcs_Sphinx_Model_Source_Interface
{
    const XML_PATH_SERVER_HOST = 'mgcs_sphinx/source/direct_host';
    const XML_PATH_SERVER_PORT = 'mgcs_sphinx/source/direct_port';

    /** @var SphinxClient */
    protected $_sphinx;
    /** @var string */
    protected $_index;

    public function __construct()
    {
        $this->_sphinx = new SphinxClient();
    }

    /**
     * Set Index
     *
     * @param string $index
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setIndex($index)
    {
        $this->_index = $index;
        return $this;
    }

    /**
     * Set Match Mode
     *
     * @param int $mode
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setMatchMode($mode)
    {
        $this->_sphinx->setMatchMode($mode);
        return $this;
    }

    /**
     * Set Field Weights
     *
     * @param array $weights
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setFieldWeights(array $weights)
    {
        $this->_sphinx->setFieldWeights($weights);
        return $this;
    }

    /**
     * Set Limits
     *
     * @param int $offset
     * @param int $limit
     * @param int $max
     * @param int $cutoff
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setLimits($offset, $limit, $max = 0, $cutoff = 0)
    {
        $this->_sphinx->setLimits($offset, $limit, $max, $cutoff);
        return $this;
    }

    /**
     * Set Ranking Mode
     *
     * @param int $ranker
     * @param string $rankexpr
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setRankingMode($ranker, $rankexpr = '')
    {
        $this->_sphinx->setRankingMode($ranker, $rankexpr);
        return $this;
    }

    /**
     * Set Sort Mode
     *
     * @param int $mode
     * @param string $sortby
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setSortMode($mode, $sortby = '')
    {
        $this->_sphinx->setSortMode($mode, $sortby);
        return $this;
    }

    /**
     * Set Index Weights
     *
     * @param array $weights
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setIndexWeights(array $weights)
    {
        $this->_sphinx->setIndexWeights($weights);
        return $this;
    }

    /**
     * Set Id Range
     *
     * @param int $min
     * @param int $max
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setIDRange($min, $max)
    {
        $this->_sphinx->setIDRange($min, $max);
        return $this;
    }

    /**
     * Run Query
     *
     * @param string $query
     * @return array
     */
    public function query($query)
    {
        $host = Mage::getStoreConfig(self::XML_PATH_SERVER_HOST);
        $port = Mage::getStoreConfig(self::XML_PATH_SERVER_PORT);
        if (empty($host)) {
            throw new Exception('Empty Sphinx server host setting');
        }
        if (empty($port)) {
            $port = 9312;
        }
        $this->_sphinx->setServer($host, $port);

        return $this->_sphinx->query($query);
    }

}