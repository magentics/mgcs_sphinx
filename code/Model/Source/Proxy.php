<?php

class Mgcs_Sphinx_Model_Source_Proxy implements Mgcs_Sphinx_Model_Source_Interface
{

    const XML_PATH_URL          = 'mgcs_sphinx/source/proxy_url';
    const XML_PATH_AUTH_ACCOUNT = 'mgcs_sphinx/source/proxy_account';
    const XML_PATH_AUTH_KEY     = 'mgcs_sphinx/source/proxy_key';

    /** @var string */
    protected $_index;

    /** @var array */
    protected $_commands;

    /**
     * Allowed commands on the client. Notice that these commands are all forced to lowercase
     * @var array
     */
    protected $_allowedCommands = array(
        'setlimits', 'setmatchmode', 'setrankingmode', 'setsortmode', 'setfieldweights', 'setindexweights',
        'setidrange', 'setfilter', 'setfilterrange', 'setfilterfloatrange', 'setgeoanchor', 'setgroupby',
        'setgroupdistinct', 'setarrayresult', 'setselect', 'buildexcerpts', 'buildkeywords',

        // Commands I don't know of whether they're safe or not
        // 'setoverride',
    );

    public function __construct()
    {
    }

    /**
     * Add a command to the list of commands to send
     *
     * @param string $command
     * @param array $args
     * @return Mgcs_Sphinx_Model_Source_Proxy
     */
    protected function _addCommand($command, $args)
    {
        $this->_commands[] = array($command, $args);
        return $this;
    }

    /**
     * Set Index
     *
     * @param string $index
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setIndex($index)
    {
        $this->_index = $index;
        return $this;
    }

    /**
     * Set Match Mode
     *
     * @param int $mode
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setMatchMode($mode)
    {
        return $this->_addCommand('setMatchMode', array($mode));
    }

    /**
     * Set Field Weights
     *
     * @param array $weights
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setFieldWeights(array $weights)
    {
        return $this->_addCommand('setFieldWeights', array($weights));
    }

    /**
     * Set Limits
     *
     * @param int $offset
     * @param int $limit
     * @param int $max
     * @param int $cutoff
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setLimits($offset, $limit, $max = 0, $cutoff = 0)
    {
        return $this->_addCommand('setLimits', array($offset, $limit, $max, $cutoff));
    }

    /**
     * Set Ranking Mode
     *
     * @param int $ranker
     * @param string $rankexpr
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setRankingMode($ranker, $rankexpr = '')
    {
        return $this->_addCommand('setRankingMode', array($ranker, $rankexpr));
    }

    /**
     * Set Sort Mode
     *
     * @param int $mode
     * @param string $sortby
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setSortMode($mode, $sortby = '')
    {
        return $this->_addCommand('setSortMode', array($mode, $sortby));
    }

    /**
     * Set Index Weights
     *
     * @param array $weights
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setIndexWeights(array $weights)
    {
        return $this->_addCommand('setIndexWeights', array($weights));
    }

    /**
     * Set Id Range
     *
     * @param int $min
     * @param int $max
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setIDRange($min, $max)
    {
        return $this->_addCommand('setIDRange', array($min, $max));
    }

    /**
     * Run Query
     *
     * @param string $query
     * @return array
     */
    public function query($query)
    {
        $url     = Mage::getStoreConfig(self::XML_PATH_URL);
        $account = Mage::getStoreConfig(self::XML_PATH_AUTH_ACCOUNT);
        $key     = Mage::getStoreConfig(self::XML_PATH_AUTH_KEY);

        if (empty($url)) {
            throw new Exception('Empty Sphinx proxy url setting');
        }
        if (empty($account)) {
            throw new Exception('Empty Sphinx proxy account setting');
        }
        if (empty($key)) {
            throw new Exception('Empty Sphinx proxy key setting');
        }

        $request = array(
            'auth'     => array(
                'account' => $account,
                'key'     => $key,
                'method'  => 'key',
            ),
            'commands' => $this->_commands,
            'query'    => $query,
        );

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER,         false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,     array('Content-type: application/json'));
        curl_setopt($curl, CURLOPT_POST,           true);
        curl_setopt($curl, CURLOPT_POSTFIELDS,     json_encode($request));

        header('Content-Type: text/plain');
        $response = json_decode(curl_exec($curl), true);
        if (is_null($response) || !is_array($response)) {
            throw new Exception('Empty response from Sphinx proxy');
        }
        if (!empty($response['error'])) {
            throw new Exception('Error response from Sphinx proxy' . (empty($response['message']) ? ': ' . $response['message'] : ''));
        }
        if (!isset($response['results'])) {
            throw new Exception('Empty results key in Sphinx proxy response');
        }
        return $response['results'];
    }

}