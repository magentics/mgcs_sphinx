<?php

interface Mgcs_Sphinx_Model_Source_Interface
{

    /**
     * Set Index
     *
     * @param string $index
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setIndex($index);

    /**
     * Set Match Mode
     *
     * @param int $mode
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setMatchMode($mode);

    /**
     * Set Field Weights
     *
     * @param array $weights
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setFieldWeights(array $weights);

    /**
     * Set Limits
     *
     * @param int $offset
     * @param int $limit
     * @param int $max
     * @param int $cutoff
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setLimits($offset, $limit, $max = 0, $cutoff = 0);

    /**
     * Set Ranking Mode
     *
     * @param int $ranker
     * @param string $rankexpr
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setRankingMode($ranker, $rankexpr = '');

    /**
     * Set Sort Mode
     *
     * @param int $mode
     * @param string $sortby
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setSortMode($mode, $sortby = '');

    /**
     * Set Index Weights
     *
     * @param array $weights
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setIndexWeights(array $weights);

    /**
     * Set Id Range
     *
     * @param int $min
     * @param int $max
     * @return Mgcs_Sphinx_Model_Source_Direct
     */
    public function setIdRange($min, $max);

    /**
     * Run Query
     *
     * @param string $query
     * @return array
     */
    public function query($query);

}