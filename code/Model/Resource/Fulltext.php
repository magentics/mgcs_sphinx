<?php

class Mgcs_Sphinx_Model_Resource_Fulltext extends Mage_CatalogSearch_Model_Resource_Fulltext
{
    const XML_PATH_INDEX = 'mgcs_sphinx/source/index';
    const XML_PATH_ACTIVE_INDEXER = 'mgcs_sphinx/active/indexer';
    const XML_PATH_ACTIVE_FRONTEND = 'mgcs_sphinx/active/frontend';

    /**
     * Init resource model
     *
     */
    protected function _construct()
    {
        // engine is only important fpr indexing
        if (!Mage::getStoreConfigFlag(self::XML_PATH_ACTIVE_INDEXER)) {
            return parent::_construct();
        }

        $this->_init('catalogsearch/fulltext', 'product_id');
        $this->_engine = Mage::helper('mgcs_sphinx')->getEngine();
    }

    /**
     * Prepare results for query
     *
     * @param Mage_CatalogSearch_Model_Fulltext $object
     * @param string $queryText
     * @param Mage_CatalogSearch_Model_Query $query
     * @return Mage_CatalogSearch_Model_Mysql4_Fulltext
     */
    public function prepareResult($object, $queryText, $query)
    {
        if (!Mage::getStoreConfigFlag(self::XML_PATH_ACTIVE_FRONTEND)) {
            return parent::prepareResult($object, $queryText, $query);
        }

        if (!$query->getIsProcessed()) {
            // process 'replace' setting
            if ($replaceDefinitions = Mage::helper('mgcs_sphinx')->getQueryReplaceDefinitions()) {
                foreach ($replaceDefinitions as $pattern => $replacement) {
                    $queryText = preg_replace($pattern, $replacement, $queryText);
                }
            }

            $sphinx = Mage::helper('mgcs_sphinx')->getSphinxClient();
            $sphinx->SetMatchMode(Mgcs_Sphinx_Model_Source::SPH_MATCH_EXTENDED2);
            //$sphinx->SetMatchMode(Mgcs_Sphinx_Model_Source::SPH_MATCH_ANY);
            $sphinx->setFieldWeights(array(
                'name'              => 7,
                'category'          => 1,
                'name_attributes'   => 1,
                'data_index'        => 3
            ));
            $sphinx->setLimits(0, 200, 1000, 5000);

            // SPH_RANK_PROXIMITY_BM25 ist default
            $sphinx->SetRankingMode(Mgcs_Sphinx_Model_Source::SPH_RANK_SPH04);

            $index = Mage::getStoreConfig(self::XML_PATH_INDEX);
            if (!$index) {
                $index = '*';
            }

            $result = $sphinx->query($queryText, $index);

            // Loop through our Sphinx results
            if ($result !== false && !empty($result['matches'])) {
                foreach ($result['matches'] as $productId => $docinfo) {
                    // Ensure we log query results into the Magento table.
                    $weight = $docinfo['weight'] / 1000;

                    try {
                        $this->_getWriteAdapter()->query("
                            INSERT INTO " . $this->getTable('catalogsearch/result') . "
                            (`query_id`, `product_id`, `relevance`)
                            VALUES
                            (:query_id, :product_id, :relevance)
                            ON DUPLICATE KEY UPDATE `relevance` = VALUES(`relevance`)
                            ", array(
                                'query_id'   => $query->getId(),
                                'product_id' => $productId,
                                'relevance'  => $weight,
                        ));
                    } catch (Zend_Db_Statement_Exception $e) {
                        /**
                         * if the sphinx index is out of date and returns product ids which are no longer in the
                         * database, integrity contraint exceptions are thrown.  we catch them here and simply
                         * skip them. All other exceptions are forwarded.
                         */
                        $message = $e->getMessage();
                        if (strpos($message, 'SQLSTATE[23000]: Integrity constraint violation') === false) {
                            throw $e;
                        }
                    }
                }
            }

            $query->setIsProcessed(1);
        }

        return $this;
    }

    /**
     * Prepare Fulltext index value for product
     *
     * @param array $indexData
     * @param array $productData
     * @param int $storeId
     * @return string
     */
    protected function _prepareProductIndex($indexData, $productData, $storeId)
    {
        if (! Mage::getStoreConfigFlag(self::XML_PATH_ACTIVE_INDEXER)) {
            return parent::_prepareProductIndex($indexData, $productData, $storeId);
        }

        $index = array();

        foreach ($this->_getSearchableAttributes('static') as $attribute) {
            $attributeCode = $attribute->getAttributeCode();

            if (isset($productData[$attributeCode])) {
                $value = $this->_getAttributeValue($attribute->getId(), $productData[$attributeCode], $storeId);
                if ($value) {
                    if (isset($index[$attributeCode])) {
                        // for grouped products
                        if (!is_array($index[$attributeCode])) {
                            $index[$attributeCode] = array($index[$attributeCode]);
                        }
                        $index[$attributeCode][] = $value;
                    } else {
                        // for other types of products
                        $index[$attributeCode] = $value;
                    }
                }
            }
        }

        foreach ($indexData as $entityId => $attributeData) {
            foreach ($attributeData as $attributeId => $attributeValue) {
                $value = $this->_getAttributeValue($attributeId, $attributeValue, $storeId);
                if (!is_null($value) && $value !== false) {
                    $attributeCode = $this->_getSearchableAttribute($attributeId)->getAttributeCode();

                    if (isset($index[$attributeCode])) {
                        $index[$attributeCode][$entityId] = $value;
                    } else {
                        $index[$attributeCode] = array($entityId => $value);
                    }
                }
            }
        }

        if (!$this->_engine->allowAdvancedIndex()) {
            $product = $this->_getProductEmulator()
                ->setId($productData['entity_id'])
                ->setTypeId($productData['type_id'])
                ->setStoreId($storeId);
            $typeInstance = $this->_getProductTypeInstance($productData['type_id']);
            if ($data = $typeInstance->getSearchableData($product)) {
                $index['options'] = $data;
            }
        }

        if (isset($productData['in_stock'])) {
            $index['in_stock'] = $productData['in_stock'];
        }

        // Renamed the method because otherwise we couldn't add a parameter to the method
        return $this->_engine->prepareEntityIndexSphinx($index, $this->_separator, $productData['entity_id']);
    }

}
