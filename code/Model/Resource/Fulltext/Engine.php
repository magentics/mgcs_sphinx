<?php

class Mgcs_Sphinx_Model_Resource_Fulltext_Engine extends Mage_CatalogSearch_Model_Resource_Fulltext_Engine
{
    /**
     * Multi add entities data to fulltext search table
     *
     * @param int $storeId
     * @param array $entityIndexes
     * @param string $entity 'product'|'cms'
     * @return Mgcs_Sphinx_Model_Resource_Fulltext_Engine
     */
    public function saveEntityIndexes($storeId, $entityIndexes, $entity = 'product')
    {
        $adapter = $this->_getWriteAdapter();
        $data	 = array();
        $storeId = (int)$storeId;
        $date    = Mage::getSingleton('core/date')->gmtDate();

        foreach ($entityIndexes as $entityId => &$index) {
            $data[] = array(
                'product_id'      => (int)$entityId,
                'store_id'        => $storeId,
                'data_index'      => $index['data_index'],
                'name'            => $index['name'],
                'name_attributes' => $index['name_attributes'],
                'category'        => $index['category'],
                'created_at'      => $date,
                'updated_at'      => $date,
            );
        }

        if ($data) {
            $adapter->insertOnDuplicate($this->getTable('mgcs_sphinx/fulltext'), $data, array(
                'data_index', 'name', 'name_attributes', 'category', 'updated_at'
            ));
        }

        return $this;
    }

    /**
     * Remove entity data from fulltext search table
     *
     * @param int $storeId
     * @param int $entityId
     * @param string $entity 'product'|'cms'
     * @return Mgcs_Sphinx_Model_Resource_Fulltext_Engine
     */
    public function cleanIndex($storeId = null, $entityId = null, $entity = 'product')
    {
        // I don't think this is necessary now we have InnoDB tables with Foreign Keys
        return;
        $where = array();

        if (!is_null($storeId)) {
            $where[] = $this->_getWriteAdapter()->quoteInto('store_id = ?', $storeId);
        }
        if (!is_null($entityId)) {
            $where[] = $this->_getWriteAdapter()->quoteInto('product_id IN (?)', $entityId);
        }

        $this->_getWriteAdapter()->delete($this->getTable('mgcs_sphinx/fulltext'), implode(' AND ', $where));

        return $this;
    }

    /**
     * Prepare index array as a string glued by separator
     *
     * Renamed the method because otherwise we couldn't add a parameter to the method
     *
     * @param array $index
     * @param string $separator
     * @return string
     */
    public function prepareEntityIndexSphinx($index, $separator = ' ', $entityId = null)
    {
        return Mage::helper('mgcs_sphinx')->prepareIndexdata($index, $separator, $entityId);
    }
}
