<?php

class Mgcs_Sphinx_Model_Source
{

    /// known match modes
    const SPH_MATCH_ALL       = 0;
    const SPH_MATCH_ANY       = 1;
    const SPH_MATCH_PHRASE    = 2;
    const SPH_MATCH_BOOLEAN   = 3;
    const SPH_MATCH_EXTENDED  = 4;
    const SPH_MATCH_FULLSCAN  = 5;
    const SPH_MATCH_EXTENDED2 = 6;    // extended engine V2 (TEMPORARY, WILL BE REMOV;

    /// known ranking modes (ext2 only)
    const SPH_RANK_PROXIMITY_BM25 = 0;    ///< default mode, phrase proximity major factor and BM25 minor ;
    const SPH_RANK_BM25           = 1;    ///< statistical mode, BM25 ranking only (faster but worse quali;
    const SPH_RANK_NONE           = 2;    ///< no ranking, all matches get a weight o;
    const SPH_RANK_WORDCOUNT      = 3;    ///< simple word-count weighting, rank is a weighted sum of per-field keyword occurence cou;
    const SPH_RANK_PROXIMITY      = 4;
    const SPH_RANK_MATCHANY       = 5;
    const SPH_RANK_FIELDMASK      = 6;
    const SPH_RANK_SPH04          = 7;
    const SPH_RANK_EXPR           = 8;
    const SPH_RANK_TOTAL          = 9;

    /// known sort modes
    const SPH_SORT_RELEVANCE     = 0;
    const SPH_SORT_ATTR_DESC     = 1;
    const SPH_SORT_ATTR_ASC      = 2;
    const SPH_SORT_TIME_SEGMENTS = 3;
    const SPH_SORT_EXTENDED      = 4;
    const SPH_SORT_EXPR          = 5;

    /// known filter types
    const SPH_FILTER_VALUES     = 0;
    const SPH_FILTER_RANGE      = 1;
    const SPH_FILTER_FLOATRANGE = 2;

    /// known attribute types
    const SPH_ATTR_INTEGER   = 1;
    const SPH_ATTR_TIMESTAMP = 2;
    const SPH_ATTR_ORDINAL   = 3;
    const SPH_ATTR_BOOL      = 4;
    const SPH_ATTR_FLOAT     = 5;
    const SPH_ATTR_BIGINT    = 6;
    const SPH_ATTR_STRING    = 7;
    const SPH_ATTR_MULTI     = 0x40000001;
    const SPH_ATTR_MULTI64   = 0x40000002;

    /// known grouping functions
    const SPH_GROUPBY_DAY      = 0;
    const SPH_GROUPBY_WEEK     = 1;
    const SPH_GROUPBY_MONTH    = 2;
    const SPH_GROUPBY_YEAR     = 3;
    const SPH_GROUPBY_ATTR     = 4;
    const SPH_GROUPBY_ATTRPAIR = 5;

}
