<?php

class Mgcs_Sphinx_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_SOURCE         = 'mgcs_sphinx/source/method';
    const XML_PATH_SYNONYMS       = 'mgcs_sphinx/search/synonyms';
    const XML_PATH_QUERY_REPLACE  = 'mgcs_sphinx/search/replace';
    const XML_PATH_INDEX_REPLACE  = 'mgcs_sphinx/indexing/replace';

    protected $_sphinxClient;

    protected $_replaceDefinitions;
    protected $_synonyms;

    public function getSphinxClient()
    {
        if (!isset($this->_sphinxClient)) {
            $sourceMethod = Mage::getStoreConfig(self::XML_PATH_SOURCE);
            $source = Mage::getModel($sourceMethod);
            if (!$source) {
                throw new Exception('Cannot instantiate search source');
            }
            $this->_sphinxClient = $source;
        }

        return $this->_sphinxClient;
    }


    /**
     * taken from https://gist.github.com/2727341
     */
    public function prepareIndexdata($index, $separator = ' ', $entityId = null)
    {
        $attributes = array();

        $dataIndex = array();
        foreach ($index as $key => $value) {
            // As long as this isn't a standard attribute use it in our
            // concatenated column.
            if (!in_array($key, array('sku', 'name', 'description', 'short_description', 'meta_keywords', 'meta_title'))) {
                $attributes[$key] = $value;
            }

            if (!is_array($value)) {
                $dataIndex[] = $value;
            } else {
                $dataIndex = array_merge($dataIndex, $value);
            }
        }

        // Get the product name.
        $name = '';
        if (isset($index['name'])) {
            if (is_array($index['name'])) {
                $name = $index['name'][$entityId]; // Use the configurable product's name
            } else {
                $name = $index['name']; // Use the simple product's name
            }
        }

        // Combine the name with each non-standard attribute
        $nameAttributes = array();
        foreach ($attributes as $code => $value) {
            if (!is_array($value)) {
                $value = array($value);
            }

            // Loop through each simple product's attribute values and assign to
            // product name.
            foreach ($value as $key => $itemValue) {
                if (isset($nameAttributes[$key])) {
                    $nameAttributes[$key] .= ' ' . $itemValue;
                } else {
                    // The first time we see this add the name to start.
                    $nameAttributes[$key] = $name . ' ' . $itemValue;
                }
            }
        }

        // Get categories
        $categorieNames = array();
        if ($entityId) {
            $productResource = Mage::getResourceSingleton('catalog/product');
            $productStub = new Varien_Object(array('id' => $entityId));
            foreach ($productResource->getCategoryCollection($productStub)->addNameToResult() as $category) {
                $categorieNames[] = $category->getName();
            }
        }

        $data = array(
            'name'			  => $this->replaceIndexingDefinitions($name),
            'name_attributes' => $this->replaceIndexingDefinitions(implode('. ', $nameAttributes)),
            'data_index'	  => $this->replaceIndexingDefinitions(implode($separator, $dataIndex)),
            'category'		  => $this->replaceIndexingDefinitions(implode('|', $categorieNames)),
        );

        return $data;
    }

    /**
     * Return text with indexing replace definitions applied
     *
     * @param string $text
     * @return string
     */
    public function replaceIndexingDefinitions($text, $store = null)
    {
        if ($replaceDefinitions = $this->getIndexingReplaceDefinitions($store)) {
            foreach ($replaceDefinitions as $pattern => $replacement) {
                $text = preg_replace($pattern, '\\0 ' . $replacement, $text);
            }
        }
        return $text;
    }

    /**
     * Return text with query replace definitions applied
     *
     * @param string $text
     * @return string
     */
    public function replaceQueryDefinitions($text, $store = null)
    {
        // process 'replace' setting
        if ($replaceDefinitions = $this->getQueryReplaceDefinitions($store)) {
            foreach ($replaceDefinitions as $pattern => $replacement) {
                $text = preg_replace($pattern, '\\0 ' . $replacement, $text);
            }
        }
        return $text;
    }

    public function getEngine()
    {
        return Mage::getResourceSingleton('mgcs_sphinx/fulltext_engine');
    }


    /**
     * Get Synonyms setting
     *
     * @param mixed $store
     * @return array
     */
    public function getSynonyms($store = null)
    {
        if (!isset($this->_synonyms)) {
            $rows = preg_split("/[\r\n]+/", Mage::getStoreConfig(self::XML_PATH_SYNONYMS, $store));
            $synonyms = array();
            foreach ($rows as $row) {
                $row = trim($row);
                if (!$row || substr($row, 0, 1) == '#') {
                    continue;
                }
                if (substr_count($row, ';') >= 1) {
                    list($word, $synonym) = explode(';', $row, 2);
                    $word = trim($word);
                    $synonym = trim($synonym);
                    $synonyms[$word] = $synonym;
                }
            }
            $this->_synonyms = $synonyms;
        }
        return $this->_synonyms;
    }


    /**
     * Get Query Replace setting
     *
     * In the form: array(pattern => replacement, ...)
     *
     * @param mixed $store
     * @return array
     */
    public function getQueryReplaceDefinitions($store = null)
    {
        if (!isset($this->_replaceDefinitions)) {
            $this->_queryReplaceDefinitions =
                $this->_parseReplaceDefinitions(Mage::getStoreConfig(self::XML_PATH_QUERY_REPLACE, $store));
        }
        return $this->_queryReplaceDefinitions;
    }

    /**
     * Get Indexing Replace setting
     *
     * In the form: array(pattern => replacement, ...)
     *
     * @param mixed $store
     * @return array
     */
    public function getIndexingReplaceDefinitions($store = null)
    {
        if (!isset($this->_replaceDefinitions)) {
            $this->_queryReplaceDefinitions =
                $this->_parseReplaceDefinitions(Mage::getStoreConfig(self::XML_PATH_INDEX_REPLACE, $store));
        }
        return $this->_queryReplaceDefinitions;
    }

    protected function _parseReplaceDefinitions($definition)
    {
        $rows = preg_split("/[\r\n]+/", $definition);
        $definitions = array();
        foreach ($rows as $row) {
            $row = trim($row);
            if (!$row || substr($row, 0, 1) == '#') {
                continue;
            }
            if (substr_count($row, '=>') >= 1) {
                list($pattern, $replacement) = explode('=>', $row, 2);
                $pattern = trim($pattern);
                $replacement = trim($replacement);
                if (substr($pattern, 0, 1) != '/') {
                    $pattern = '/' . str_replace('/', '\\/', $pattern) . '/i';
                }
                $definitions[$pattern] = $replacement;
            }
        }
        return $definitions;
    }

}
