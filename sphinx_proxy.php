<?php

require_once 'lib/sphinxapi.php';

$body = json_decode(file_get_contents('php://input'), true);

try {
    $proxy = new Sphinx_Proxy($body);
    $response = $proxy->runQuery();
    if ($response) {
        $result = array(
            'error'   => false,
            'results' => $response,
        );
    } else {
        throw new Exception('No response from proxy');
    }
} catch (Exception $e) {
    $result = array('error' => true, 'message' => $e->getMessage(), 'code' => $e->getCode());
}

header('Content-Type: application/json; charset=UTF-8');

$options = null;
if (!empty($_GET['pretty'])) {
    $options = JSON_PRETTY_PRINT;
}
echo json_encode($result, $options);

class Sphinx_Proxy
{
    const VERSION = '0.1';

    protected $_request;
    protected $_index;
    protected $_client;

    protected $_server = '127.0.0.1';

    protected $_accounts = array(
        '{{account_name}}' => array('key' => '{{key}}', 'index' => '{{index_name}}'),
    );

    /**
     * Allowed commands on the client. Notice that these commands are all forced to lowercase
     * @var array
     */
    protected $_allowedCommands = array(
        'setlimits', 'setmatchmode', 'setrankingmode', 'setsortmode', 'setfieldweights', 'setindexweights',
        'setidrange', 'setfilter', 'setfilterrange', 'setfilterfloatrange', 'setgeoanchor', 'setgroupby',
        'setgroupdistinct', 'setarrayresult', 'setselect', 'buildexcerpts', 'buildkeywords',

        // Commands I don't know of whether they're safe or not
        // 'setoverride',
    );

    public function __construct($request)
    {
        $this->_request = $request;
        if (!$this->_request) {
            throw new Exception('Empty request');
        }
    }

    protected function _authenticate()
    {
        if (empty($this->_request['auth'])) {
            throw new Exception('Empty authentication');
        }

        $auth = $this->_request['auth'];
        if (empty($auth['method'])) {
            throw new Exception('Empty auth method');
        }
        if (empty($auth['account'])) {
            throw new Exception('Empty auth account');
        }
        if (empty($auth['key'])) {
            throw new Exception('Empty auth key');
        }
        if ($auth['method'] != 'key') {
            throw new Exception('Unrecognized auth method');
        }

        if (empty($this->_accounts[$auth['account']])) {
            throw new Exception('Unknown account');
        }

        $account = $this->_accounts[$auth['account']];
        if ($auth['key'] != $account['key']) {
            throw new Exception('Wrong key for account');
        }

        $this->_setIndex($account['index']);
    }

    protected function _setIndex($index)
    {
        $this->_index = $index;
        return $this;
    }

    protected function _getIndex()
    {
        return $this->_index;
    }

    /**
     * Get Sphinx Client
     *
     * @return SphinxClient
     */
    protected function _getClient()
    {
        if (!isset($this->_client)) {
            $client = new SphinxClient();
            $client->setServer($this->_server);
            $this->_client = $client;
        }
        return $this->_client;
    }

    protected function _callClientCommands()
    {
        $client = $this->_getClient();
        if (empty($this->_request['commands'])) {
            return;
        }
        if (!is_array($this->_request['commands'])) {
            throw new Exception('Commands need to be an array');
        }
        foreach ($this->_request['commands'] as $command) {
            if (!is_array($command)) {
                throw new Exception('Command needs to be an array');
            }
            $commandName = $command[0];
            $arguments = array();
            if (isset($command[1])) {
                $arguments = $command[1];
            }
            $commandName = strtolower($commandName);
            if (is_scalar($arguments)) {
                $arguments = array($arguments);
            }
            if (!is_array($arguments)) {
                throw new Exception('Wrong arguments for command ' . $commandName);
            }
            $arguments = array_values($arguments);
            if (!in_array($commandName, $this->_allowedCommands)) {
                throw new Exception('Denied command: ' . preg_replace('/[^a-z0-9]/i', '', $commandName));
            }
            call_user_func_array(array($client, $commandName), $arguments);
        }
    }

    protected function _runQuery()
    {
        if (empty($this->_request['query'])) {
            return;
        }

        $query = $this->_request['query'];
        return $this->_getClient()->query($query, $this->_getIndex());
    }

    public function runQuery()
    {
        $this->_authenticate();
        $this->_callClientCommands();
        $result = $this->_runQuery();

        if (!$result) {
            throw new Exception('Error while running query');
        }

        return $result;
    }

}